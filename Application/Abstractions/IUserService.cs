﻿using System;
using System.Threading.Tasks;

using OF.FuturesTrader.Application.Domain;
using OF.FuturesTrader.Data.States;

namespace OF.FuturesTrader.Application.Abstractions
{
	public interface IUserService
	{
		Task<User> GetUserByEmailAsync(string email);

		Task<User> GetByIdAsync(Guid id, bool isTracking = false);

		Task<bool> IsUserExistingAsync(string email);

		Task CreateUserAsync(
			string email,
			string password,
			string firstName,
			string lastName,
			string address,
			int? age,
			GenderType? gender,
			HearAboutUsResourceType? hearAboutUsSource);
	}
}
