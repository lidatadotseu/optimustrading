﻿using System.Collections.Generic;
using System.Threading.Tasks;

using OF.FuturesTrader.Data.States;

namespace OF.FuturesTrader.Application.Abstractions
{
	public interface ITradingClientsService
	{
		Task<List<TradingClient>> GetLatestTradingDataAsync();

		Task UpdateTradingDataAsync(List<TradingClient> tradingClients);
	}
}