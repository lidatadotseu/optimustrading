﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using OF.FuturesTrader.Application.Abstractions;
using OF.FuturesTrader.Application.Domain;
using OF.FuturesTrader.Data.Adapter;
using OF.FuturesTrader.Data.States;
using OF.FuturesTrader.ExternalServices.RythmicApi;
using OF.FuturesTrader.ExternalServices.RythmicApi.Domain;

namespace OF.FuturesTrader.Application.Services
{
	public class TradingClientsService : ITradingClientsService
	{
		private readonly IDbContextFactory<FuturesTraderDBContext> _dbContextFactory;
		private readonly IRythmicApiClient _rythmicApiClient;

		public TradingClientsService(
			IDbContextFactory<FuturesTraderDBContext> dbContextFactory,
			IRythmicApiClient rythmicApiClient)
		{
			_dbContextFactory = dbContextFactory;
			_rythmicApiClient = rythmicApiClient;
		}

		public async Task<List<TradingClient>> GetLatestTradingDataAsync()
		{
			var tradingData = await _rythmicApiClient.GetTradingDataAsync();

			var clients = MapToTradingClients(tradingData);

			return clients;
		}

		public async Task UpdateTradingDataAsync(List<TradingClient> tradingClients)
		{
			using var dbContext = _dbContextFactory.CreateDbContext();

			var oldPnlOrders = dbContext
				.Set<PNLOrder>()
				.Where(x => tradingClients.Select(x => x.Id).Any(y => y == x.ClientId));

			dbContext.RemoveRange(oldPnlOrders);

			await dbContext.SaveChangesAsync();

			await dbContext.BulkMergeAsync(tradingClients, options => options.IncludeGraph = true);
		}

		private List<TradingClient> MapToTradingClients(TradingData tradingData)
		{
			return tradingData
				.Accounts
				.Select(a =>
					new TradingClient()
					{
						Algorithm = a.RmsInfo.Algorithm,
						AutoLiquidate = a.RmsInfo.AutoLiquidate,
						AutoLiquidateCriteria = a.RmsInfo.AutoLiquidateCriteria,
						AutoLiquidateMaxMinAccountBalance = a.RmsInfo.AutoLiquidateMaxMinAccountBalance,
						AutoLiquidateThreshold = a.RmsInfo.AutoLiquidateThreshold,
						BuyLimit = a.RmsInfo.BuyLimit,
						CheckMinAccountBalance = a.RmsInfo.CheckMinAccountBalance,
						Currency = a.RmsInfo.Currency,
						DisableOnAutoLiquidate = a.RmsInfo.DisableOnAutoLiquidate,
						FcmId = a.FcmId,
						IbId = a.IbId,
						Id = a.AccountId,
						LossLimit = a.RmsInfo.LossLimit,
						MaxOrderQty = a.RmsInfo.MaxOrderQty,
						MinAccountBalance = a.RmsInfo.MinAccountBalance,
						MinMarginBalance = a.RmsInfo.MinMarginBalance,
						Name = a.AccountName,
						Orders = tradingData
									.PnlsByAccount[a.AccountId]
									.Select(x => new PNLOrder()
									{
										Symbol = x.Symbol,
										Exchange = x.Exchange,
										AccountBalance = x.AccountBalance.Use ? x.AccountBalance.Value : null,
										AvailableBuyingPower = x.AvailableBuyingPower.Use ? x.AvailableBuyingPower.Value : null,
										AvgOpenFillPrice = x.AvgOpenFillPrice.Use ? x.AvgOpenFillPrice.Value : null,
										BuyQty = x.BuyQty.Use ? x.BuyQty.Value : null,
										CashOnHand = x.CashOnHand.Use ? x.CashOnHand.Value : null,
										ClientId = a.AccountId,
										ClosedPnl = x.ClosedPnl.Use ? x.ClosedPnl.Value : null,
										ImpliedMarginReserved = x.ImpliedMarginReserved.Use ? x.ImpliedMarginReserved.Value : null,
										ImpliedNetQty = x.ImpliedNetQty.Use ? x.ImpliedNetQty.Value : null,
										MarginBalance = x.MarginBalance.Use ? x.MarginBalance.Value : null,
										OpenPnl = x.OpenPnl.Use ? x.OpenPnl.Value : null,
										Position = x.Position.Use ? x.Position.Value : null,
										PossOutOfOrder = x.PossOutOfOrder,
										ReservedBuyingPower = x.ReservedBuyingPower.Use ? x.ReservedBuyingPower.Value : null,
										ReservedMargin = x.ReservedMargin.Use ? x.ReservedMargin.Value : null,
										SellQty = x.SellQty.Use ? x.SellQty.Value : null,
										UsedBuyingPower = x.UsedBuyingPower.Use ? x.UsedBuyingPower.Value : null,
										Type = x.BuyQty.Use && x.BuyQty.Value > 0 ? (int)TransactionType.Buy : (int)TransactionType.Sell
									})
									.ToList()
					})
				.ToList();
		}
	}
}
