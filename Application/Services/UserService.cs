﻿using System;
using System.Threading.Tasks;

using BCrypt.Net;

using Microsoft.EntityFrameworkCore;

using OF.FuturesTrader.Application.Abstractions;
using OF.FuturesTrader.Application.Domain;
using OF.FuturesTrader.Data.Adapter;
using OF.FuturesTrader.Data.States;

namespace OF.FuturesTrader.Application.Services
{
	public class UserService : IUserService
	{
		private readonly IDbContextFactory<FuturesTraderDBContext> _dbContextFactory;

		public UserService(IDbContextFactory<FuturesTraderDBContext> dbContextFactory)
		{
			_dbContextFactory = dbContextFactory;
		}

		public async Task CreateUserAsync(
			string email,
			string password,
			string firstName,
			string lastName,
			string address,
			int? age,
			GenderType? gender,
			HearAboutUsResourceType? hearAboutUsSource)
		{
			var isUserExisting = await IsUserExistingAsync(email);
			if (isUserExisting)
			{
				throw new ArgumentException($"Email '{email}' is already taken");
			}

			var user = new User
			{
				Email = email,
				PasswordHash = BCrypt.Net.BCrypt.HashPassword(password),
				FirstName = firstName,
				LastName = lastName,
				Address = address,
				Age = age,
				Gender = (int?)gender.Value ?? null,
				HearAboutUsSource = (int?)hearAboutUsSource.Value ?? null
			};

			using var dbContext = _dbContextFactory.CreateDbContext();

			await dbContext
				.Set<User>()
				.AddAsync(user);

			await dbContext.SaveChangesAsync();
		}

		public async Task<User> GetUserByEmailAsync(string email)
		{
			using var dbContext = _dbContextFactory.CreateDbContext();

			var user = await dbContext
				.Set<User>()
				.AsNoTracking()
				.FirstOrDefaultAsync(x => x.Email == email);

			return user;
		}

		public async Task<User> GetByIdAsync(Guid id, bool isTracking = false)
		{
			using var dbContext = _dbContextFactory.CreateDbContext();

			var user = isTracking ?
				await dbContext
					.Set<User>()
					.FirstOrDefaultAsync(x => x.Id == id) :
				await dbContext
					.Set<User>()
					.AsNoTracking()
					.FirstOrDefaultAsync(x => x.Id == id);

			return user;
		}

		public async Task<bool> IsUserExistingAsync(string email)
		{
			using var dbContext = _dbContextFactory.CreateDbContext();

			var isUserExisting = await dbContext
				.Set<User>()
				.AnyAsync(x => x.Email == email);

			return isUserExisting;
		}
	}
}
