﻿namespace OF.FuturesTrader.Application.Domain
{
	public enum TransactionType
	{
		Buy = 1,
		Sell = 2
	}
}
