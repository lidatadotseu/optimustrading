﻿namespace OF.FuturesTrader.Application.Domain
{
	public enum HearAboutUsResourceType
	{
		SearchEngines = 1,
		SocialMedia = 2,
		WebSite = 3,
		Other = 4
	}
}
