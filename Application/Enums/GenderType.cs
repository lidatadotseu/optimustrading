﻿namespace OF.FuturesTrader.Application.Domain
{
	public enum GenderType
	{
		Male = 1,
		Female = 2,
		NotSpecified = 3
	}
}
