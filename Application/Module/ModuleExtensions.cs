﻿using System;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using OF.FuturesTrader.Application.Abstractions;
using OF.FuturesTrader.Application.Services;
using OF.FuturesTrader.Data.Module;
using OF.FuturesTrader.ExternalServices.Module;

namespace OF.FuturesTrader.Application.Module
{
    public static class ModuleExtensions
    {
        public static IServiceCollection AddApplicationModule(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDataModule(configuration);
            services.AddExternalServicesModule(configuration);

            services.AddSingleton<ITradingClientsService, TradingClientsService>();
            services.AddSingleton<IUserService, UserService>();

            return services;
        }

        public static void ApplicationPostSetup(this IServiceProvider services)
        {
            services.DataPostSetup();
        }
    }
}
