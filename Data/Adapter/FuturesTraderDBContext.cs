﻿using Microsoft.EntityFrameworkCore;

using OF.FuturesTrader.Data.States;

namespace OF.FuturesTrader.Data.Adapter
{
	public class FuturesTraderDBContext : DbContext
	{
		public FuturesTraderDBContext(DbContextOptions<FuturesTraderDBContext> options)
	: base(options)
		{
		}

		public DbSet<TradingClient> TradingClients { get; set; }

		public DbSet<PNLOrder> PNLOrders { get; set; }

		public DbSet<User> Users { get; set; }

		public DbSet<ResetPasswordToken> ResetPasswordTokens { get; set; }

		public void EnsureDBMigrated()
		{
			Database.Migrate();
			Database.EnsureCreated();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.EnableSensitiveDataLogging();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>()
				.ToTable("tbl_Users")
				.HasKey(a => a.Id);

			modelBuilder.Entity<User>()
				.Property(a => a.Id)
				.ValueGeneratedOnAdd();

			modelBuilder.Entity<User>()
				.HasIndex(x => x.Email)
				.IsUnique();

			modelBuilder.Entity<ResetPasswordToken>()
				.Property(a => a.Id)
				.ValueGeneratedOnAdd();

			modelBuilder.Entity<ResetPasswordToken>()
				.HasOne(x => x.User)
				.WithMany(x => x.ResetPasswordTokens)
				.HasForeignKey(x => x.UserId)
				.HasPrincipalKey(x => x.Id);

			modelBuilder.Entity<TradingClient>()
				.ToTable("tbl_TradingClient")
				.HasKey(a => a.Id);

			modelBuilder.Entity<PNLOrder>()
				.ToTable("tbl_PNLOrder")
				.HasKey(a => new { a.ClientId, a.Exchange, a.Symbol });

			modelBuilder.Entity<PNLOrder>().HasIndex(x => x.ClientId);
			modelBuilder.Entity<PNLOrder>().HasIndex(x => x.Id);
			modelBuilder.Entity<PNLOrder>().HasIndex(x => new { x.ClientId, x.Exchange, x.Symbol });

			modelBuilder.Entity<PNLOrder>()
				.Property(b => b.UpdatedOn)
				.HasDefaultValueSql("NOW()");

			modelBuilder.Entity<PNLOrder>()
				.Property(b => b.Id)
				.ValueGeneratedOnAdd();

			modelBuilder.Entity<PNLOrder>()
				.HasOne(x => x.Client)
				.WithMany(x => x.Orders)
				.HasForeignKey(x => x.ClientId)
				.HasPrincipalKey(x => x.Id);
		}
	}
}
