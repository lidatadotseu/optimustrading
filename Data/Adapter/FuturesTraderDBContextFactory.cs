﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace OF.FuturesTrader.Data.Adapter
{
    public class FuturesTraderDBContextFactory : IDesignTimeDbContextFactory<FuturesTraderDBContext>
    {
        public FuturesTraderDBContext CreateDbContext(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.Development.json", optional: false)
                .Build();

            var futuresTraderConnectionString = config.GetConnectionString(DBConsts.FUTURES_TRADER_DB_NAME);

            var optionsBuilder = new DbContextOptionsBuilder<FuturesTraderDBContext>();

            optionsBuilder.UseNpgsql(futuresTraderConnectionString);

            return new FuturesTraderDBContext(optionsBuilder.Options);
        }
    }
}
