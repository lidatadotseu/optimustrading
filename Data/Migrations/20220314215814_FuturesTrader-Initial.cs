﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace OF.FuturesTrader.Data.Migrations
{
    public partial class FuturesTraderInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tbl_TradingClient",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    FcmId = table.Column<string>(type: "text", nullable: true),
                    IbId = table.Column<string>(type: "text", nullable: true),
                    Algorithm = table.Column<string>(type: "text", nullable: true),
                    Currency = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<string>(type: "text", nullable: true),
                    CheckMinAccountBalance = table.Column<bool>(type: "boolean", nullable: false),
                    LossLimit = table.Column<double>(type: "double precision", nullable: false),
                    MinAccountBalance = table.Column<double>(type: "double precision", nullable: false),
                    MinMarginBalance = table.Column<double>(type: "double precision", nullable: false),
                    BuyLimit = table.Column<int>(type: "integer", nullable: false),
                    MaxOrderQty = table.Column<int>(type: "integer", nullable: false),
                    SellLimit = table.Column<int>(type: "integer", nullable: false),
                    AutoLiquidate = table.Column<string>(type: "text", nullable: true),
                    AutoLiquidateCriteria = table.Column<string>(type: "text", nullable: true),
                    AutoLiquidateThreshold = table.Column<double>(type: "double precision", nullable: false),
                    AutoLiquidateMaxMinAccountBalance = table.Column<double>(type: "double precision", nullable: false),
                    DisableOnAutoLiquidate = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_TradingClient", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tbl_Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    Email = table.Column<string>(type: "text", nullable: true),
                    Address = table.Column<string>(type: "text", nullable: true),
                    Age = table.Column<int>(type: "integer", nullable: true),
                    Gender = table.Column<int>(type: "integer", nullable: true),
                    HearAboutUsSource = table.Column<int>(type: "integer", nullable: true),
                    PasswordHash = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tbl_PNLOrder",
                columns: table => new
                {
                    ClientId = table.Column<string>(type: "text", nullable: false),
                    Exchange = table.Column<string>(type: "text", nullable: false),
                    Symbol = table.Column<string>(type: "text", nullable: false),
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AccountBalance = table.Column<double>(type: "double precision", nullable: true),
                    AvgOpenFillPrice = table.Column<double>(type: "double precision", nullable: true),
                    MarginBalance = table.Column<double>(type: "double precision", nullable: true),
                    ReservedMargin = table.Column<double>(type: "double precision", nullable: true),
                    AvailableBuyingPower = table.Column<double>(type: "double precision", nullable: true),
                    ReservedBuyingPower = table.Column<double>(type: "double precision", nullable: true),
                    UsedBuyingPower = table.Column<double>(type: "double precision", nullable: true),
                    ClosedPnl = table.Column<double>(type: "double precision", nullable: true),
                    OpenPnl = table.Column<double>(type: "double precision", nullable: true),
                    CashOnHand = table.Column<double>(type: "double precision", nullable: true),
                    Position = table.Column<int>(type: "integer", nullable: true),
                    BuyQty = table.Column<int>(type: "integer", nullable: true),
                    SellQty = table.Column<int>(type: "integer", nullable: true),
                    ImpliedNetQty = table.Column<int>(type: "integer", nullable: true),
                    ImpliedMarginReserved = table.Column<double>(type: "double precision", nullable: true),
                    PossOutOfOrder = table.Column<bool>(type: "boolean", nullable: false),
                    UpdatedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValueSql: "NOW()"),
                    Type = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_PNLOrder", x => new { x.ClientId, x.Exchange, x.Symbol });
                    table.ForeignKey(
                        name: "FK_tbl_PNLOrder_tbl_TradingClient_ClientId",
                        column: x => x.ClientId,
                        principalTable: "tbl_TradingClient",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ResetPasswordTokens",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Token = table.Column<string>(type: "text", nullable: true),
                    ExpireOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UsedOn = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResetPasswordTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ResetPasswordTokens_tbl_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "tbl_Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ResetPasswordTokens_UserId",
                table: "ResetPasswordTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_PNLOrder_ClientId",
                table: "tbl_PNLOrder",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_PNLOrder_ClientId_Exchange_Symbol",
                table: "tbl_PNLOrder",
                columns: new[] { "ClientId", "Exchange", "Symbol" });

            migrationBuilder.CreateIndex(
                name: "IX_tbl_PNLOrder_Id",
                table: "tbl_PNLOrder",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_Users_Email",
                table: "tbl_Users",
                column: "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ResetPasswordTokens");

            migrationBuilder.DropTable(
                name: "tbl_PNLOrder");

            migrationBuilder.DropTable(
                name: "tbl_Users");

            migrationBuilder.DropTable(
                name: "tbl_TradingClient");
        }
    }
}
