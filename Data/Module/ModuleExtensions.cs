﻿using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using OF.FuturesTrader.Data.Adapter;

using Z.EntityFramework.Extensions;

namespace OF.FuturesTrader.Data.Module
{
	public static class ModuleExtensions
	{
		public static IServiceCollection AddDataModule(this IServiceCollection services, IConfiguration configuration)
		{
			var futuresTraderConnectionString = configuration.GetConnectionString(DBConsts.FUTURES_TRADER_DB_NAME);

			services.AddDbContextFactory<FuturesTraderDBContext>(
				options => options.UseNpgsql(futuresTraderConnectionString));

			return services;
		}

		public static void DataPostSetup(this IServiceProvider services)
		{
			var dbContextFactory = services.GetService<IDbContextFactory<FuturesTraderDBContext>>();

			EntityFrameworkManager.ContextFactory = context => dbContextFactory.CreateDbContext();
		}
	}
}
