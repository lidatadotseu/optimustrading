﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace OF.FuturesTrader.Data.States
{
	public class User
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public int? Age { get; set; }

        public int? Gender { get; set; }

        public int? HearAboutUsSource { get; set; }

        public IEnumerable<ResetPasswordToken> ResetPasswordTokens { get; set; }

        [JsonIgnore]
        public string PasswordHash { get; set; }
    }
}
