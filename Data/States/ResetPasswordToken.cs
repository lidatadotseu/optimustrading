﻿using System;

namespace OF.FuturesTrader.Data.States
{
	public class ResetPasswordToken
	{
		public Guid Id { get; set; }

		public Guid UserId { get; set; }

		public User User { get; set; }

		public string Token { get; set; }

		public DateTime ExpireOn { get; set; }

		public DateTime UsedOn { get; set; }
	}
}
