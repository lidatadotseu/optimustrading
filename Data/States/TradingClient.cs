﻿using System.Collections.Generic;

namespace OF.FuturesTrader.Data.States
{
	public class TradingClient
	{
		public string Id { get; set; }

		public string Name { get; set; }

		public string FcmId { get; set; }

		public string IbId { get; set; }

		public string Algorithm { get; set; }

		public string Currency { get; set; }

		public string Status { get; set; }

		public bool CheckMinAccountBalance { get; set; }

		public double LossLimit { get; set; }

		public double MinAccountBalance { get; set; }

		public double MinMarginBalance { get; set; }

		public int BuyLimit { get; set; }

		public int MaxOrderQty { get; set; }

		public int SellLimit { get; set; }

		public string AutoLiquidate { get; set; }

		public string AutoLiquidateCriteria { get; set; }

		public double AutoLiquidateThreshold { get; set; }

		public double AutoLiquidateMaxMinAccountBalance { get; set; }

		public string DisableOnAutoLiquidate { get; set; }

		public List<PNLOrder> Orders { get; set; }
	}
}
