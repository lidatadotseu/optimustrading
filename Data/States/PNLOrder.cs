﻿using System;

namespace OF.FuturesTrader.Data.States
{
	public class PNLOrder
	{
		public long Id { get; set; }

		public TradingClient Client { get; set; }

		public string ClientId { get; set; }

		public string Exchange { get; set; }

		public string Symbol { get; set; }

		public double? AccountBalance { get; set; }

		public double? AvgOpenFillPrice { get; set; }

		public double? MarginBalance { get; set; }

		public double? ReservedMargin { get; set; }

		public double? AvailableBuyingPower { get; set; }

		public double? ReservedBuyingPower { get; set; }

		public double? UsedBuyingPower { get; set; }

		public double? ClosedPnl { get; set; }

		public double? OpenPnl { get; set; }

		public double? CashOnHand { get; set; }

		public int? Position { get; set; }

		public int? BuyQty { get; set; }

		public int? SellQty { get; set; }

		public int? ImpliedNetQty { get; set; }

		public double? ImpliedMarginReserved { get; set; }

		public bool PossOutOfOrder { get; set; }

		public DateTime UpdatedOn { get; set; }

		public int Type { get; set; }
	}
}
