using System;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using OF.FuturesTrader.Api.Authorization;
using OF.FuturesTrader.Api.Exceptions;
using OF.FuturesTrader.Api.Module;
using OF.FuturesTrader.Api.Workers;
using OF.FuturesTrader.Application.Module;
using OF.FuturesTrader.Data.Adapter;
using OF.FuturesTrader.ExternalServices.RythmicApi.Settings;

using Serilog;
using Serilog.Sinks.Elasticsearch;

namespace OF.FuturesTrader.Api
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var app = SetupApplication(args);

			app.Run();
		}

		private static WebApplication SetupApplication(string[] args)
		{
			var builder = WebApplication.CreateBuilder(args);

			var config = BuildConfiguration();

			builder.Services.AddApplicationModule(config);

			AddServices(builder.Services);

			AddSettings(builder.Services, config);

			SetupElasticSearch(builder, config);

			builder.Services.AddControllers(options =>
				options.Filters.Add(typeof(HttpResponseExceptionFilter)));

			// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
			builder.Services.AddEndpointsApiExplorer();
			builder.Services.AddSwaggerGen();

			builder.Services.AddHostedService<UpdateTradingClientsWorker>();

			var app = builder.Build();

			WebApplicationPostSetup(app);

			return app;
		}

		private static IConfigurationRoot BuildConfiguration()
		{
			return new ConfigurationBuilder()
							.AddJsonFile("appsettings.json", optional: false)
							.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: false)
							.AddEnvironmentVariables()
							.Build();
		}

		private static void AddServices(IServiceCollection services)
		{
			services.AddSingleton<IAuthService, AuthService>();
		}

		private static void AddSettings(IServiceCollection services, IConfiguration configuration)
		{
			services.Configure<RythmicApiClientSettings>(
				configuration.GetSection(RythmicApiClientSettings.Key));
			services.Configure<ElasticSearchSettings>(
				configuration.GetSection(ElasticSearchSettings.Key));
			services.Configure<SecuritySettings>(
				configuration.GetSection(SecuritySettings.Key));
		}

		private static void SetupElasticSearch(WebApplicationBuilder builder, IConfiguration config)
		{
			var esOptions = new ElasticSearchSettings();
			config.GetSection(ElasticSearchSettings.Key).Bind(esOptions);

			builder.Host.UseSerilog((ctx, lc) =>
				lc.WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(esOptions.Url))
				{
					AutoRegisterTemplate = true,
					IndexFormat = esOptions.IndexFormat
				}));
		}

		private static void WebApplicationPostSetup(WebApplication app)
		{
			app.Services.ApplicationPostSetup();

			if (app.Environment.IsDevelopment())
			{
				using (var scope = app.Services.CreateScope())
				{
					var context = scope.ServiceProvider.GetRequiredService<FuturesTraderDBContext>();

					context.EnsureDBMigrated();
				}

				app.UseSwagger();
				app.UseSwaggerUI();
			}

			app.UseMiddleware<JwtMiddleware>();

			app.MapControllers();
		}
	}
}

