﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using OF.FuturesTrader.Api.Authorization;
using OF.FuturesTrader.Api.Dtos.Users;
using OF.FuturesTrader.Application.Abstractions;
using OF.FuturesTrader.Application.Domain;

namespace OF.FuturesTrader.Api.Controllers
{
	[Route("api/users")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		private readonly IUserService _userService;
		private readonly IAuthService _authService;

		public UsersController(
			IUserService userService,
			IAuthService authService)
		{
			_userService = userService;
			_authService = authService;
		}

		[HttpPost]
		public async Task<IActionResult> PostAsync([FromBody] UserCreateRequestDto request)
		{
			await _userService.CreateUserAsync(
				request.Email,
				request.Password,
				request.FirstName,
				request.LastName,
				request.Address,
				request.Age,
				(GenderType?)request.Gender.Value ?? null,
				(HearAboutUsResourceType?)request.HearAboutUsSource.Value ?? null);

			return Ok();
		}

		[HttpGet("login")]
		public async Task<ActionResult<LoginResponseDto>> LoginAsync(
			[FromQuery] [Required] [EmailAddress] string email,
			[FromQuery] [Required] string password)
		{
			var response = await _authService.AuthenticateAsync(email, password);

			return Ok(response);
		}


		[HttpGet("test-authorization")]
		[Authorize]
		public IActionResult TestAuthorization()
		{
			return Ok();
		}
	}
}
