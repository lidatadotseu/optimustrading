﻿using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;

using OF.FuturesTrader.Application.Abstractions;

namespace OF.FuturesTrader.Api.Authorization
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;

        public JwtMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IUserService userService, IAuthService authService)
        {
            var token = context.Request.Headers["Authorization"]
                .FirstOrDefault()
                ?.Split(" ")
                .Last();
            var userId = authService.ValidateToken(token);
            if (userId != null)
            {
                context.Items["User"] = await userService.GetByIdAsync(userId.Value);
            }

            await _next(context);
        }
    }
}
