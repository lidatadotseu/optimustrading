﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using OF.FuturesTrader.Api.Dtos.Users;
using OF.FuturesTrader.Api.Module;
using OF.FuturesTrader.Application.Abstractions;
using OF.FuturesTrader.Data.States;

using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace OF.FuturesTrader.Api.Authorization
{
	public class AuthService : IAuthService
	{
		private readonly IUserService _userService;
		private readonly SecuritySettings _securitySettings;

		public AuthService(
			IUserService userService,
			IOptions<SecuritySettings> securitySettings)
		{
			_userService = userService;
			_securitySettings = securitySettings.Value;
		}

		public async Task<LoginResponseDto> AuthenticateAsync(string email, string password)
		{
			var user = await _userService.GetUserByEmailAsync(email);

			if (user == null || !BCrypt.Net.BCrypt.Verify(password, user.PasswordHash))
			{
				throw new UnauthorizedAccessException("Username or password is incorrect");
			}

			var response = new LoginResponseDto()
			{
				Id = user.Id,
				FirstName = user.FirstName,
				LastName = user.LastName,
				Email = user.Email,
				Token = GenerateToken(user)
			};

			return response;
		}

		public Guid? ValidateToken(string token)
		{
			if (token == null)
			{
				return null;
			}

			try
			{
				var tokenHandler = new JwtSecurityTokenHandler();
				var key = Encoding.ASCII.GetBytes(_securitySettings.Secret);

				tokenHandler.ValidateToken(
					token,
					new TokenValidationParameters
					{
						ValidateIssuerSigningKey = true,
						IssuerSigningKey = new SymmetricSecurityKey(key),
						ValidateIssuer = false,
						ValidateAudience = false,
						ClockSkew = TimeSpan.Zero
					},
					out SecurityToken validatedToken);

				var jwtToken = (JwtSecurityToken)validatedToken;
				var userId = Guid.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

				return userId;
			}
			catch
			{
				return null;
			}
		}

		private string GenerateToken(User user)
		{
			var tokenHandler = new JwtSecurityTokenHandler();

			var key = Encoding.ASCII.GetBytes(_securitySettings.Secret);
			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Subject = new ClaimsIdentity(new[]
				{
					new Claim("id", user.Id.ToString())
				}),
				Expires = DateTime.UtcNow.AddDays(7),
				SigningCredentials = new SigningCredentials(
					new SymmetricSecurityKey(key),
					SecurityAlgorithms.HmacSha256Signature)
			};
			var token = tokenHandler.CreateToken(tokenDescriptor);

			return tokenHandler.WriteToken(token);
		}
	}
}
