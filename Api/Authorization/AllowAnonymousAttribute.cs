﻿using System;

namespace OF.FuturesTrader.Api.Authorization
{
	[AttributeUsage(AttributeTargets.Method)]
	public class AllowAnonymousAttribute : Attribute
	{ 
	}
}
