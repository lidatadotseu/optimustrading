﻿using System;
using System.Threading.Tasks;

using OF.FuturesTrader.Api.Dtos.Users;

namespace OF.FuturesTrader.Api.Authorization
{
	public interface IAuthService
	{
        Task<LoginResponseDto> AuthenticateAsync(string email, string password);

		Guid? ValidateToken(string token);
	}
}
