﻿using System.ComponentModel.DataAnnotations;

namespace OF.FuturesTrader.Api.Dtos.Users
{
	public class UserCreateRequestDto
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public int? Age { get; set; }

        public int? Gender { get; set; }

        public int? HearAboutUsSource { get; set; }
    }
}
