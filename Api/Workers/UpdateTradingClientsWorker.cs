﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using OF.FuturesTrader.Application.Abstractions;

namespace OF.FuturesTrader.Api.Workers
{
	public class UpdateTradingClientsWorker : BackgroundService
	{
		private const int UpdateTradingDataPeriod = 10 * 1000;
		private readonly ILogger<UpdateTradingClientsWorker> _logger;
		private readonly ITradingClientsService _tradingClientsService;

		public UpdateTradingClientsWorker(
			ILogger<UpdateTradingClientsWorker> logger,
			ITradingClientsService tradingClientsService)
		{
			_logger = logger;
			_tradingClientsService = tradingClientsService;
		}

		protected override async Task ExecuteAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation($"Start UpdateTradingClientsWorker.");

			await UpdateTradingDataProcessAsync(stoppingToken);
		}

		private async Task UpdateTradingDataProcessAsync(CancellationToken stoppingToken)
		{
			while (!stoppingToken.IsCancellationRequested)
			{
				try
				{
					_logger.LogInformation("Start updating trading data.");

					var tradingData = await _tradingClientsService.GetLatestTradingDataAsync();

					await _tradingClientsService.UpdateTradingDataAsync(tradingData);

					_logger.LogInformation("Complete updating trading data.");
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, "Error occurred trying to update trading data.");
				}

				await Task.Delay(UpdateTradingDataPeriod, stoppingToken);
			}
		}

		public override async Task StopAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("Stop UpdateTradingClientsWorker.");

			await base.StopAsync(stoppingToken);
		}
	}
}
