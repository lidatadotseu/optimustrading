﻿namespace OF.FuturesTrader.Api.Module
{
	public class SecuritySettings
	{
		public const string Key = nameof(SecuritySettings);

		public string Secret { get; set; }
	}
}
