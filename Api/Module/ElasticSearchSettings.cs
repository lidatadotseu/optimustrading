﻿namespace OF.FuturesTrader.Api.Module
{
	public class ElasticSearchSettings
	{
		public const string Key = nameof(ElasticSearchSettings);

		public string Url { get; set; }

		public string IndexFormat { get; set; }
	}
}
