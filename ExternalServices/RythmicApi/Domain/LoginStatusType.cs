﻿namespace OF.FuturesTrader.ExternalServices.RythmicApi.Domain
{
	public enum LoginStatusType
	{
		LoginComplete = 1,
		LoginFailed = 2
	}
}
