﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using com.omnesys.rapi;

using Microsoft.Extensions.Logging;

namespace OF.FuturesTrader.ExternalServices.RythmicApi.Domain
{
	public class TradingData : RCallbacks
	{
		private readonly ILogger<TradingData> _logger;
		private readonly int _accountWaitingTimeout = 10 * 1000;
		private ManualResetEventSlim _accountsLoaded;
		private ManualResetEventSlim _pnlsLoaded;
		private int _pnlsPerAccountLoadedCount = 0;

		public TradingData(ILogger<TradingData> logger)
		{
			_logger = logger;
			_accountsLoaded = new(false);
			_pnlsLoaded = new(false);
		}

		public List<AccountInfo> Accounts { get; set; }

		public Dictionary<string, List<PnlInfo>> PnlsByAccount { get; set; }

		public LoginStatusType Status { get; private set; }

		public override void AccountList(AccountListInfo accountListInfo)
		{
			Accounts = accountListInfo.Accounts.ToList();
			PnlsByAccount = Accounts.ToDictionary(x => x.AccountId, x => new List<PnlInfo>());
			_pnlsPerAccountLoadedCount = 0;
			_accountsLoaded.Set();
		}

		public override void Alert(AlertInfo alertInfo)
		{
			if (alertInfo.ConnectionId == ConnectionId.Repository)
			{
				if (alertInfo.AlertType == AlertType.LoginComplete)
				{
					Status = LoginStatusType.LoginComplete;

					_logger.LogInformation("Repository login successfull");
				}
				else if (alertInfo.AlertType == AlertType.LoginFailed)
				{
					Status = LoginStatusType.LoginFailed;

					_logger.LogInformation("Repository login failed");
				}
			}
		}

		public override void PnlReplay(PnlReplayInfo pnlReplayInfo)
		{
			PnlsByAccount[pnlReplayInfo.Account.AccountId] = 
				pnlReplayInfo
					.PnlInfoList
					.Where(x => x.Account != null && x.Symbol != null && x.Exchange != null)
					.ToList();

			lock (_pnlsLoaded)
			{
				++_pnlsPerAccountLoadedCount;
				if (_pnlsPerAccountLoadedCount == PnlsByAccount.Count)
				{
					_pnlsLoaded.Set();
				}
			}
		}

		public async Task<List<AccountInfo>> GetAccountsAsync()
		{
			if (Status == LoginStatusType.LoginFailed)
			{
				throw new LoginFailedException();
			}

			var areAccountsLoaded = await Task.Run(() => _accountsLoaded.Wait(_accountWaitingTimeout));

			if (areAccountsLoaded)
			{
				return Accounts;
			}
			else
			{
				throw new TimeoutException();
			}
		}

		public async Task<Dictionary<string, List<PnlInfo>>> GetPnlsAsync()
		{
			if (Status == LoginStatusType.LoginFailed)
			{
				throw new LoginFailedException();
			}

			var arePnlsLoaded = await Task.Run(() => _pnlsLoaded.Wait(_accountWaitingTimeout));

			if (arePnlsLoaded)
			{
				return PnlsByAccount;
			}
			else
			{
				throw new TimeoutException();
			}
		}
	}
}
