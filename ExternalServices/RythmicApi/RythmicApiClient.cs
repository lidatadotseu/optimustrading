﻿using System;
using System.Threading.Tasks;

using com.omnesys.rapi;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using OF.FuturesTrader.ExternalServices.RythmicApi.Domain;
using OF.FuturesTrader.ExternalServices.RythmicApi.Settings;

namespace OF.FuturesTrader.ExternalServices.RythmicApi
{
	public class RythmicApiClient : IRythmicApiClient
	{
		private readonly RythmicApiClientSettings _settings;
		private readonly ILoggerFactory _loggerFactory;

		public RythmicApiClient(
			IOptions<RythmicApiClientSettings> rythmicApiClientSettings,
			ILoggerFactory loggerFactory)
		{
			_settings = rythmicApiClientSettings.Value;
			_loggerFactory = loggerFactory;
		}

		public async Task<TradingData> GetTradingDataAsync()
		{
			var tradingData = new TradingData(_loggerFactory.CreateLogger<TradingData>());

			var rEngine = new REngine(new REngineParams()
			{
				AdmCnnctPt = _settings.REngine.AdmCnnctPt,
				AppName = _settings.REngine.AppName,
				AppVersion = _settings.REngine.AppVersion,
				DmnSrvrAddr = _settings.REngine.DmnSrvrAddr,
				DomainName = _settings.REngine.DomainName,
				LicSrvrAddr = _settings.REngine.LicSrvrAddr,
				LocBrokAddr = _settings.REngine.LocBrokAddr,
				LogFilePath = _settings.REngine.LogFilePath,
				LoggerAddr = _settings.REngine.LoggerAddr,
				AdmCallbacks = new REngineAdmCallbacks()
			});

			rEngine.loginRepository(tradingData,
					 _settings.REngineLoginRepository.User,
					 _settings.REngineLoginRepository.Password,
					 _settings.REngineLoginRepository.CnnctPt);

			rEngine.login(tradingData,
					 _settings.REngineLogin.MdUser,
					 _settings.REngineLogin.MdPassword,
					 _settings.REngineLogin.MdCnnctPt,
					 _settings.REngineLogin.MdUser,
					 _settings.REngineLogin.MdPassword,
					 _settings.REngineLogin.TsCnnctPt,
					 _settings.REngineLogin.PnlCnnctPt,
					 string.Empty,
					 string.Empty,
					 string.Empty);

			var accounts = await tradingData.GetAccountsAsync();

			foreach (var account in accounts)
			{
				rEngine.replayPnl(account, null);
			}
			
			await tradingData.GetPnlsAsync();

			return tradingData;
		}
	}
}
