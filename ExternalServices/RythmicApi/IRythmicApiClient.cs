﻿using System.Threading.Tasks;

using OF.FuturesTrader.ExternalServices.RythmicApi.Domain;

namespace OF.FuturesTrader.ExternalServices.RythmicApi
{
	public interface IRythmicApiClient
	{
		Task<TradingData> GetTradingDataAsync();
	}
}