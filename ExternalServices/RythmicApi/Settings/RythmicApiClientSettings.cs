﻿namespace OF.FuturesTrader.ExternalServices.RythmicApi.Settings
{
	public class RythmicApiClientSettings
	{
		public const string Key = nameof(RythmicApiClientSettings);

		public REngineSettings REngine { get; set; }

		public REngineLoginRepositorySettings REngineLoginRepository { get; set; }

		public REngineLoginSettings REngineLogin { get; set; }
	}
}
