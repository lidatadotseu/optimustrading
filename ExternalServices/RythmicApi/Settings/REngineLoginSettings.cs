﻿namespace OF.FuturesTrader.ExternalServices.RythmicApi.Settings
{
	public class REngineLoginSettings
	{
		public string MdUser { get; set; }

		public string MdPassword { get; set; }

		public string MdCnnctPt { get; set; }

		public string User { get; set; }

		public string Password { get; set; }

		public string TsCnnctPt { get; set; }

		public string PnlCnnctPt { get; set; }
	}
}
