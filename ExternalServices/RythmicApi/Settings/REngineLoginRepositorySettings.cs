﻿namespace OF.FuturesTrader.ExternalServices.RythmicApi.Settings
{
	public class REngineLoginRepositorySettings
	{
		public string User { get; set; }

		public string Password { get; set; }

		public string CnnctPt { get; set; }
	}
}
