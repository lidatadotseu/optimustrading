﻿namespace OF.FuturesTrader.ExternalServices.RythmicApi.Settings
{
	public class REngineSettings
	{
		public string AdmCnnctPt { get; set; }

		public string AppName { get; set; }

		public string AppVersion { get; set; }

		public string DmnSrvrAddr { get; set; }

		public string DomainName { get; set; }

		public string LicSrvrAddr { get; set; }

		public string LocBrokAddr { get; set; }

		public string LoggerAddr { get; set; }

		public string LogFilePath { get; set; }
	}
}
