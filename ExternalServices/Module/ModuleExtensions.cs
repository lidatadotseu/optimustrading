﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using OF.FuturesTrader.ExternalServices.RythmicApi;

namespace OF.FuturesTrader.ExternalServices.Module
{
	public static class ModuleExtensions
	{
		public static IServiceCollection AddExternalServicesModule(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddSingleton<IRythmicApiClient, RythmicApiClient>();

			return services;
		}
	}
}
